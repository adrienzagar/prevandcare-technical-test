import React from 'react'

import Logo from './Logo/Logo.jsx'
import TabButton from './Tab/TabButton.jsx'
import DropDownMenu from './DropDownMenu/DropDownMenu.jsx'
import './index.css'

const Header = () => {
    return (
        <header className="navbar">
            <div className="navbar__logo">
                <Logo />
            </div>
            <div className="navbar__tab">
                <TabButton title='care manager' />
                <TabButton title='ressources' />
                <DropDownMenu />
            </div>
        </header>
    )
}

export default Header
