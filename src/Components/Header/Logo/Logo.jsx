// === NPM import ===
import React from 'react'

// === Local import ===
import PrevAndCare from '../../../assets/logo.png'
import './index.css'

const Logo = () => {
    return (
        <a href="/">
            <img src={PrevAndCare} alt='PrevAndCare' className='logo'></img>
        </a>
    )
}

export default Logo
