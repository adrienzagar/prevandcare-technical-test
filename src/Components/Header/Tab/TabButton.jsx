// === NPM import ===
import React from 'react'

// === Local import ===
import './index.css'

const TabButton = ({ title }) => {
    return (
        <div className="button">
            <h1 className="button__title">
                {title}
            </h1>
        </div>
    )
}

export default TabButton
