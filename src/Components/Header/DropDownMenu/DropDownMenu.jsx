// === NPM import ===
import React from 'react'

// === Local import ===
import './index.css'
import arrow from '../../../assets/arrow.png'

const DropDownMenu = () => {
    return (
        <div className="dropdown">
            <span className="dropdown__circle">MR</span>
            <h1 className="dropdown__title"> mon compte</h1>
            <img src={arrow} alt="" className="dropdown__arrow" />
        </div>
    )
}

export default DropDownMenu
