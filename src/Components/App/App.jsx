// import logo from './logo.svg';
import './index.css';

import Header from '../Header/Header.jsx'
import Main from '../Main/Main'
import Footer from '../Footer/Footer';

const App = () => {
  return (
    <div className="App">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
