import React from 'react'

import './index.css'
import Button from './Button/Button'

const Footer = () => {
    return (
        <footer className="footer">
            <Button text="besoin d'aide ?" className={'footer__button'} />
        </footer>
    )
}

export default Footer
