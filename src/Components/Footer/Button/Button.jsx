import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ text, className, onCLick }) => {
    return (
        <button className={className} onClick={onCLick}>
            {text}
        </button>
    )
}

Button.propTypes = {
    text: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func
}

export default Button
