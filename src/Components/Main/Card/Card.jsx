// === NPM import ===
import React from 'react'
import PropTypes from 'prop-types'

// === Local import ===
import CardTitle from './CardTitle/CardTitle.jsx'
import CardSubtitle from './CardSubtitle/CardSubtitle.jsx'

import './index.css'



const Card = ({ data, children }) => {
    
    // console.log(data);
    return (
        // JSX Fragment 
        <>
            {data.map((data, index) => (
                // Method to display each data imported 
                <div key={index} className="card">
                    <div className="card__container">
                        <span className="card__photo"></span>
                        <div className="card__descritpion">
                            <CardTitle text={data.text} caremanager={data.caremanager} about={data.about} client={data.client}  />
                            <CardSubtitle subtitle={data.subtitle} date={data.date} />
                        </div>
                    </div>
                    <div className="card__buttonlist">
                        {children}
                    </div>
                </div>
            ))};
        </>
    )
}

Card.propTypes = {
    client: PropTypes.string,
    text: PropTypes.string,
    caremanager : PropTypes.string,
    about: PropTypes.string,
    subtitle : PropTypes.string,
    date: PropTypes.string,
    children : PropTypes.array,
}

export default Card
