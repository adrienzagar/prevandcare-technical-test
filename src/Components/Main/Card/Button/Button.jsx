// === NPM import ===
import React from 'react'
import PropTypes from 'prop-types'

// === Local import ===
import './Button.css'

const Button = ({ text, color }) => {
    return (
        <button className={`card__button card__button-${color}`}>
            <p className="card__button card__text">{text}</p>
        </button>
    )
}

Button.propTypes = {
    text : PropTypes.string,
    color : PropTypes.string
}

export default Button
