// === NPM import ===
import React from 'react'
import PropTypes from 'prop-types'

// === Local import ===
import './index.css'

const CardSubtitle = ({ subtitle, date }) => {
    return (
        <div className='card__subtitles'>
            <h2>
                <small className='card__date'>
                    {date}
                </small>
            </h2>
            <h2>
                <small className='card__subtitle'>
                    {subtitle}
                </small>
            </h2>
        </div>
    )
};

CardSubtitle.propTypes = {
    subtitle : PropTypes.string,
    date: PropTypes.string,
}

export default CardSubtitle
