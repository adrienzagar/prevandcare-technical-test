// === NPM import ===
import React from 'react'
import PropTypes from 'prop-types'

// === Local import ===
import './CardTitle.css'

const CardTitle = ({ text , caremanager, about, client }) => {
    return (
        <h1 className="card__description-title ">
            <span className="card__title-black">{ text }</span>
            <span className="card__title-pink">{ caremanager }</span>
            <span className="card__title-black">{ about }</span>
            <span className="card__title-pink">{client}</span>
        </h1>
    )
}

CardTitle.propTypes = {
    client: PropTypes.string,
    text: PropTypes.string,
    caremanager : PropTypes.string,
    about: PropTypes.string,
}

export default CardTitle
