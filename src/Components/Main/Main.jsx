// === NPM import ===
import React from 'react'

// === Local import ===
import './index.css'
import data from '../../data'
import Title from './Title/Title.jsx'
import Subtitle from './Subtitle/Subtitle.jsx'
import AddButton from './AddButton/AddButton.jsx'
import Card from './Card/Card'
import Button from './Card//Button/Button.jsx'

const Body = () => {
    return (
        <main className="main">
            <section className="main__section">
                <div className="main__title">
                    <Title />
                    <Subtitle />
                </div>
                <div className="main__addbutton">
                    <AddButton />
                </div>
            </section>
            <div className="card__container"></div>
            <Card
                data={data}
            >
                <Button text="1 nouveau message" color="green"/>
                <Button text="5 documents" color="lightgreen"/>
                <Button text="plan d'aide " color="pink" />
            </ Card>
            {/* <Button /> */}
        </main>
    )
}

export default Body
