// === NPM import ===
import React from 'react'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';

// === local import
import ModalButton from '../../../Footer/Button/Button'

const Modal = () => {
    return (
        <Popup pup trigger={<button className="button addbutton hover"> + </button>} modal nested>
            {close => (
                <div className="modal">
                        <div className="header"> Messagerie Instantannée </div>
                        <div className="content">
                            Nos caremanager sont à votre service.
                            <br/>
                            <br/>
                            Prenez contact avec l'un d'entre eux dés maintenant.
                        </div>
            
                    <div className="actions">
                        <span
                            className="button"
                            onClick={() => {
                            close();
                            }}
                        >
                            <ModalButton
                                text='retour'
                                className={'modal__bouton-back'}
                            />
                        </span>
                        <span
                            className="button"
                            onClick={() => {
                                close();
                            }}
                        >
                            <ModalButton
                                text='envoyer un message'
                                className={'modal__button-send'}
                            />
                        </span>
                    </div>
                </div>
            )}
          </Popup>
    )
}

export default Modal
