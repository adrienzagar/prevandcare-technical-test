// === NPM import ===
import React from 'react'

// === Local import ===
import './index.css'

const Subtitle = () => {
    return (
        <div className='main__subtitle'>
            On explique des trucs.
        </div>
    )
}

export default Subtitle
