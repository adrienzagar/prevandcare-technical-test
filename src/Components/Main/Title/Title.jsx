// === NPM import ===
import React from 'react'

// === Local import ===
import './index.css'

const Title = () => {
    return (
        <div className='main__title'>
            <span className='main__title-suffix'>mes</span> conversasions (1)
        </div>
    )
}

export default Title
